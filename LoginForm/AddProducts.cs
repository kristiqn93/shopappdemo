﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace LoginForm
{
    public partial class AddProductsForm : DevExpress.XtraEditors.XtraForm
    {
        public AddProductsForm()
        {
            InitializeComponent();
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            using (var db = new ShopStorageDatabase())
            {
                var product = db.Products.Create();

                var price = txtPrice.Text;
                decimal pric;

                if (decimal.TryParse(price, out pric) &&
                    txtBarcode.Text.Length > 3 && txtProductName.Text.Length > 3)
                {
                    product.Name = txtProductName.Text;
                    product.Barcode = txtBarcode.Text;
                    product.Price = pric;

                    db.Products.Add(product);
                    db.SaveChanges();
                    this.Close();

                    MessageBox.Show("Succesfull adding product.", "Success",
                        MessageBoxButtons.OK, MessageBoxIcon.None);

                   
                }
                else
                {
                    txtPrice.Text = null;
                    txtBarcode.Text = null;
                    txtProductName.Text = null;
                    MessageBox.Show("Incorect entered fields ", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}