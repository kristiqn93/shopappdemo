﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Native;
using  DevExpress.XtraTreeList.Nodes;
using ComboBox = System.Windows.Forms.ComboBox;

namespace LoginForm
{
    public partial class ShopForm : DevExpress.XtraEditors.XtraForm
    {
        public ShopForm()
        {
            InitializeComponent();

            comboBox.Items.Add("BGN");
            comboBox.Items.Add("USD");
            comboBox.Items.Add("EUR");
            comboBox.Text = "BGN";
            using (var db = new ShopStorageDatabase())
            {
                var barcode = db.Products.Select(p => p.Barcode).ToList();
                lookupBarcode.Properties.DataSource = barcode;
            }
        }
        public void AdminRights()
        {
            btnRegister.Visible = true;
            btnAddProducts.Visible = true;
        }
        public void UserRights()
        {
            btnAddProducts.Visible = false;
            btnRegister.Visible = false;
        }
        public decimal totalprice;
        public decimal total;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtChange.Text = null;
            string combo = comboBox.Text;
            string barcode = lookupBarcode.Text;

            using (var db = new ShopStorageDatabase())
            {
                Currency currency = new Currency();
                currency = db.Currencies.FirstOrDefault(cu => cu.Code == combo);
                Product p = new Product();
                p = db.Products.FirstOrDefault(pr => pr.Barcode == barcode);


                if (p != null && currency != null)
                {
                    TreeListNode treeNode = treeList.AppendNode(null, null);
                    treeNode.SetValue("Product", p.Name);
                    treeNode.SetValue("Price", p.Price);
                    totalprice += p.Price;
                    total = totalprice/currency.Factor;
                    total = decimal.Round(total, 2);
                    txtPrice.Text = total.ToString() + " " + currency.Code;
                    lookupBarcode.Text = null;
                    string s = lookupBarcode.Text;

                }
                else
                {
                    MessageBox.Show("The product is not found", "Not found!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lookupBarcode.Text = null;
                }
            }
        }
        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string combo = comboBox.Text;

            using (var db = new ShopStorageDatabase())
            {
                Currency currency = new Currency();
                currency = db.Currencies.FirstOrDefault(cu => cu.Code == combo);
                if (combo != null)
                {
                    total = (totalprice/currency.Factor);
                    total = decimal.Round(total, 2);
                    txtPrice.Text = total + " " + currency.Code;
                }
            }
        }
        private void lookupBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtChange.Text = null;
                string barcode = lookupBarcode.Text;
                string combo = comboBox.Text;

                using (var db = new ShopStorageDatabase())
                {
                    Product p = new Product();
                    p = db.Products.FirstOrDefault(pr => pr.Barcode == barcode);
                    Currency currency = new Currency();
                    currency = db.Currencies.FirstOrDefault(cu => cu.Code == combo);

                    if (p != null && currency != null)
                    {
                        TreeListNode treeNode = treeList.AppendNode(null, null);
                        treeNode.SetValue("Product", p.Name);
                        treeNode.SetValue("Price", p.Price);
                        totalprice += p.Price;
                        total = totalprice/currency.Factor;
                        total = decimal.Round(total, 2);
                        txtPrice.Text = total.ToString() + " " + currency.Code;
                        lookupBarcode.Text = null;
                    }
                    else
                    {
                        MessageBox.Show("The product is not found", "Not found!",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        lookupBarcode.Text = null;
                    }
                }
            }
        }
        private void btnBuy_Click(object sender, EventArgs e)
        {
            string cash = txtCash.Text;
            string change = txtCash.Text;
            decimal c, dec;

            if (cash != null)
                if (decimal.TryParse(cash, out c))
                {

                    dec = c - total;
                    dec = decimal.Round(dec, 2);
                    change = dec.ToString();
                }
                else
                {
                    MessageBox.Show("Invalid input", "Invalid",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            txtChange.Text = change;
            txtCash.Text = null;
            txtPrice.Text = null;
            total = 0;
            totalprice = 0;
            treeList.ClearNodes();
        }

        private void txtCash_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string cash = txtCash.Text;
                string change = txtCash.Text;
                decimal c, dec;

                if (cash != null)
                    if (decimal.TryParse(cash, out c))
                    {

                        dec = c - total;
                        dec = decimal.Round(dec, 2);
                        change = dec.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Invalid input", "Invalid",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                txtChange.Text = change;
                txtCash.Text = null;
                txtPrice.Text = null;
                total = 0;
                totalprice = 0;
                treeList.ClearNodes();
            }
        }
        private void ShopForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            var register = new RegisterForm();
            register.ShowDialog();

        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            var login = new LoginForm();
            this.Hide();
            login.Show();
           
        }

        private void btnAddProducts_Click(object sender, EventArgs e)
        {
            var addproduct = new AddProductsForm();
            addproduct.ShowDialog();
        }
    }
}